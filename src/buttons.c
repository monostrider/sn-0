// Copyright: (c) 2023 monostrider (https://monostrider.com)

#include <hardware/pio.h>
#include <pico/stdlib.h> /* temp */
#include <stdio.h> /* temp */
#include "buttons.h"
#include "matrix.pio.h"

volatile uint32_t matrix_bit_states;

void buttons_setup() {
	unsigned int offset = pio_add_program(pio0, &matrix_program);
	unsigned int sm = pio_claim_unused_sm(pio0, true);

	matrix_program_init(pio0, sm, offset, ROWS_PIN_BASE, COLS_PIN_BASE, 60);

	pio_sm_set_enabled(pio0, sm, true);

	printf("starting matrix scanning\n");
	matrix_bit_states = 0;
	while (true) {
		matrix_bit_states = pio_sm_get_blocking(pio0, sm);
	}
}
