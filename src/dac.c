// Copyright: (c) 2023 monostrider (https://monostrider.com)

#include <hardware/dma.h>
#include <hardware/pio.h>
#include <stdio.h> /* temp */
#include "audio.h"
#include "dac.h"
#include "dac_clock.pio.h"
#include "dac_data.pio.h"

volatile int32_t dac_output_buffer[STEREO * 2]; /* double buffering */
volatile int32_t* dac_output_blocks[2]; /* seperate first and second half */

int dac_chan_data;
int dac_chan_addresser;

//volatile unsigned int writeable_half; /* either 1 or 0 */

void dac_handlebuffer() {
	/* the buffer that is currently not being read by the data channel */
	if (*(int32_t**)dma_hw->ch[dac_chan_addresser].read_addr
			== dac_output_buffer) {
		writeable_output_block = dac_output_blocks[0];
		audio_generatebuffer();
	} else {
		writeable_output_block = dac_output_blocks[1];
		audio_generatebuffer();
	}

	/* clear the interrupt bit at dac_chan_data */
	dma_hw->ints0 = 1u << dac_chan_data;
}

static void (*handler)(/* int32_t* */); /* for dumb compiler reasons */

void dac_setup() {
	/* claim statemachine for data program and add program to PIO
	 * instruction memory */
	int sm_data = pio_claim_unused_sm(pio0, true);
	unsigned int offset_data = pio_add_program(pio0, &dac_data_program);
	dac_data_program_init(pio0, sm_data, offset_data, DAC_PIN_SIGNAL,
		DAC_PIN_SELECT, SAMPLE_RATE);
	pio_sm_clear_fifos(pio0, sm_data);
	pio_sm_restart(pio0, sm_data);
	
	/* the first and second half of the double buffer */
	dac_output_blocks[0] = &dac_output_buffer[FIRST_HALF];
	dac_output_blocks[1] = &dac_output_buffer[SECOND_HALF];
	
	/* configure addresser dma channel for data statemachine, which will
	 * basically just (re)set the data read address */
	dac_chan_addresser = dma_claim_unused_channel(true);
	dma_channel_config config_addresser = dma_channel_get_default_config(
		dac_chan_addresser);
	channel_config_set_read_increment(&config_addresser, true); /*
	essentially switch between first and second half of double buffer */
	channel_config_set_write_increment(&config_addresser, false);
	channel_config_set_ring(&config_addresser, false, 3); /* false: apply
	wrap to read addresses, size of address wrap region indicated with bits
	*/
	channel_config_set_transfer_data_size(&config_addresser, DMA_SIZE_32);
	channel_config_set_high_priority(&config_addresser, true);

	/* configure data dma channel for data statemachine */
	dac_chan_data = dma_claim_unused_channel(true);
	dma_channel_config config_data = dma_channel_get_default_config(
		dac_chan_data);
	channel_config_set_read_increment(&config_data, true); /* increment
	trough all samples in buffer */
	channel_config_set_write_increment(&config_data, false);
	channel_config_set_transfer_data_size(&config_data, DMA_SIZE_32);
	channel_config_set_dreq(&config_data,
		pio_get_dreq(pio0, sm_data, true)); /* true: send data, the dreq
		is the only way that this channel will trigger again */
	channel_config_set_chain_to(&config_data, dac_chan_addresser);
	channel_config_set_high_priority(&config_data, true);

	//writeable_half = 1; /* this is essential */
	handler = &dac_handlebuffer;
	
	/* configure the processor to run data_block_finished() when the data
	 * DMA channel finishes */
	dma_channel_set_irq0_enabled(dac_chan_data, true); /* raise irq0 when
	the data channel finishes a block */
	irq_set_exclusive_handler(DMA_IRQ_0, (irq_handler_t) handler);
	irq_set_priority(DMA_IRQ_0, 255);
	irq_set_enabled(DMA_IRQ_0, true);
	
	/* apply addresser dma configuration and but do not start dma process */
	dma_channel_configure(dac_chan_addresser, &config_addresser,
		&dma_hw->ch[dac_chan_data].al3_read_addr_trig, /* destination,
		the read address for the data channel, it also reloads the data
		channel counter and then triggers it on write */
		dac_output_blocks, /* source, the pointer passed here is where
		the blocks start in memory */
		1, /* increment to the second block, possibly wrap back to first
		block */
		false /* do not start yet */);

	/* apply data dma configuration and start dma process */
	dma_channel_configure(dac_chan_data, &config_data,
		&pio0->txf[sm_data], /* destination, the pio's fifo */
		NULL, /* source, will be set by addreser */
		STEREO, /* max. transfer count, transfer both audio channels,
		assert interrupt after this, and chain to addresser */
		false /* do not start yet */);

	/* claim statemachine for clock program and add program to PIO
	 * instruction memory */
	int sm_clock = pio_claim_unused_sm(pio0, true);
	unsigned int offset_clock = pio_add_program(pio0, &dac_clock_program);
	dac_clock_program_init(pio0, sm_clock, offset_clock, DAC_PIN_CLOCK,
		SAMPLE_RATE);

	/* enable both the data and the clock statemachine */
	dma_channel_start(dac_chan_addresser);
	pio_enable_sm_mask_in_sync(pio0, (1u << sm_data) | (1u << sm_clock));
}
