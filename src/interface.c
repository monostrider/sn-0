// Copyright: (c) 2023 monostrider (https://monostrider.com)

#include <hardware/clocks.h> /* temp */
#include <pico/multicore.h>
#include <stdio.h>
#include "buttons.h"
#include "encoders.h"
#include "pixels.h"
#include "interface.h"

void interface_forkprocessing() {
	/* multicore_fifo_push_blocking(123); */

	printf("Hello from core 1!\n");

	/*
	uint32_t g = multicore_fifo_pop_blocking();
	if (g != 123) {
		printf("That's not right on core 1!\n");
	} else {
		printf("All has gone well on core 1.\n");
	}

	printf("recieved on core 1: %u\n", g);
	*/

	encoders_setup();
	pixels_setup();
	buttons_setup(); /* noreturn */
	printf("OOPS!\n");
}
