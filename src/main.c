// Copyright: (c) 2023 monostrider (https://monostrider.com)

#include <hardware/clocks.h>
#include <pico/multicore.h>
#include <pico/stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include "audio.h"
#include "interface.h"

int main() {
	set_sys_clock_khz(132000, true);
	stdio_init_all();
	sleep_ms(2500);

	printf("initializing\n");
	printf("forking processes\n\n");

	audio_forkprocessing(); /* already on core 0*/
	printf("forked audio processes\n\n");
	multicore_launch_core1(interface_forkprocessing);
	printf("forked interface processes\n\n");
 
	printf("all proccesses have ended\n");

	while (true) {
		/* do nothing, if there is nothing to do */
	}

	printf("returning with exit code 0\n");

	return 0;
}
