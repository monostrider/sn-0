// Copyright: (c) 2023 monostrider (https://monostrider.com)

.program pixels

.side_set 1 /* keeps holding value until set otherwise */

.wrap_target
tail:
	out x, 1      side 0 [2] /* halting here results in a latch */
	jmp !x, _zero side 1 [1]
_one:
	jmp tail      side 1 [4] /* extend positive pulse */
_zero:
	nop           side 0 [4] /* extend tail */
.wrap

% c-sdk {
	#include <hardware/clocks.h>
	
	static inline void pixels_program_init(PIO pio, unsigned int sm,
			unsigned int offset, unsigned int pin,
			unsigned int clk_baud /* = 800000 */) {
		/* make configuration */
		pio_sm_config cfg = pixels_program_get_default_config(offset);
		sm_config_set_sideset_pins(&cfg, pin);
		sm_config_set_out_shift(&cfg, false, true, 24); /* autopull */
		sm_config_set_fifo_join(&cfg, PIO_FIFO_JOIN_TX);

		/* configure clock */
		const unsigned int fixed_cycles = 2 + 5 + 3; /* per bit */
		float div = (float) clk_baud * fixed_cycles;
		sm_config_set_clkdiv(&cfg,
			(float) clock_get_hz(clk_sys) / div);

		/* apply configuration */
		pio_sm_init(pio, sm, offset, &cfg);

		/* initialize gpio pins */
		pio_gpio_init(pio, pin);
		uint32_t mask = (1u << pin);
		pio_sm_set_pins_with_mask(pio, sm, 0, mask); /* clear */
		pio_sm_set_pindirs_with_mask(pio, sm, mask, mask);
	}
%}
