// Copyright: (c) 2023 monostrider (https://monostrider.com)

#include <hardware/clocks.h> /* temp */
#include <pico/multicore.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h> /* temp */
#include "dac.h"
#include "encoders.h"

float lerp(float a, float b, float f) {
	return a * (1.0f - f) + (b * f);
}

/* testing */
float audio_carrier_freq = 440.0f;
float audio_mod_depth = 0.0f;
float audio_ratio = 0.25f;
float audio_amplitude = 14000000.0f;

volatile int32_t* writeable_output_block;

/* this gets called every two samples, hence the multiplying with STEREO */
void audio_generatebuffer() {
	static float carrier = 0; /* carrier sinewave */
	static float carrier_phase = 0;
	static float mod = 0; /* modulator sinewave */
	static float mod_phase = 0;

	/* control values */
	//audio_carrier_freq = 440.0f + (float) encoders_values[0] * 20.0f;
//	audio_mod_depth = 2.0f + (float) encoders_values[1] * 0.2f;
	audio_ratio = 0.25f + (float) encoders_values[2] * 0.25f;

	mod_phase += (2 * M_PI * (audio_carrier_freq * audio_ratio)
		/ SAMPLE_RATE) * STEREO;
	mod_phase = fmodf(mod_phase, M_PI * 2);

	carrier_phase += (2 * M_PI * audio_carrier_freq / SAMPLE_RATE)
		* STEREO;
	carrier_phase = fmodf(carrier_phase, M_PI * 2);

	mod = sinf(mod_phase) * audio_mod_depth;
	carrier = ((sinf(fmodf(carrier_phase + mod, M_PI * 2))
		* audio_amplitude));


	/* for stereo purposes, to form one frame */
	writeable_output_block[0] = (int32_t) carrier; /* left channel */
	writeable_output_block[1] = (int32_t) carrier; /* right channel */
}

void audio_forkprocessing() {
	printf("Hello from core 0!\n");

	/*
	uint32_t g = multicore_fifo_pop_blocking();
	if (g != 123) {
		printf("Problem with core 0!\n");
	} else {
		multicore_fifo_push_blocking(123);
		printf("All has gone well on core 0.\n");
	}
	*/

	dac_setup(); /* already core 0 */

}
