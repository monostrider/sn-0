// Copyright: (c) 2023 monostrider (https://monostrider.com)

#include <hardware/clocks.h> /* temp*/
#include <hardware/dma.h>
#include <hardware/pio.h>
#include <pico/stdlib.h> /* temp */
#include <pico/time.h> /* semi-temp */
#include <stdio.h> /* temp */
#include <stdlib.h>
#include "audio.h" /* temp */
#include "pixels.pio.h"
#include "pixels.h"

#include "buttons.h" /* ultra temp */

volatile uint32_t pixels_output_buffer[PIXELS];

static inline uint32_t rgb(uint8_t r, uint8_t g, uint8_t b) {
	return ((uint32_t) (r) << 8)
		| ((uint32_t) (g) << 16)
		| (uint32_t) (b);
}

/* FIXME XXX TODO THIS IS VERY HACKY */
void pixels_generatebuffer() {
	for (int i = 0; i < PIXELS; i++) {
		pixels_output_buffer[i] = rgb(0, 0, ((audio_mod_depth - 1) * 17)) << 8u;
	}

//	unsigned int temp = ((unsigned int) (audio_ratio * 4.0f) % 16);
//	pixels_output_buffer[temp] = rgb(
//		audio_carrier_freq, audio_mod_depth * 10, 0) << 8u;

float lrp(float a, float b, float f) {
	return a * (1.0f - f) + (b * f);
}
	audio_amplitude = lrp(audio_amplitude, 0, 0.001f);
	audio_mod_depth = lrp(audio_mod_depth, 1, 0.01f);
	for (int i = 0; i < 16; i++) {
		unsigned int p = i;
		if (i > 7) { p = 15 - (i - 8); }
		if (((matrix_bit_states >> i) & 0x01) != 1) {
		pixels_output_buffer[p] = rgb(255, 255, 255) << 8u;
		}


		if (((matrix_bit_states >> i) & 0x01) != 1) {
		audio_amplitude = 14000000.0f;
		audio_mod_depth = 4;
		switch (i) {
		case 8:  audio_carrier_freq = 261; break;
		case 1:  audio_carrier_freq = 277; break;
		case 9:  audio_carrier_freq = 293; break;
		case 2:  audio_carrier_freq = 311; break;
		case 10: audio_carrier_freq = 329; break;
		case 11: audio_carrier_freq = 349; break;
		case 4:  audio_carrier_freq = 369; break;
		case 12: audio_carrier_freq = 392; break;
		case 5:  audio_carrier_freq = 415; break;
		case 13: audio_carrier_freq = 440; break;
		case 6:  audio_carrier_freq = 466; break;
		case 14: audio_carrier_freq = 493; break;
		case 15: audio_carrier_freq = 523; break;
		}
		}
		
	}
}

int chan_pixels;

int64_t alarm(alarm_id_t id, void *user_data) {
	/* set read address and also trigger */
	dma_channel_set_read_addr(chan_pixels, &pixels_output_buffer[0], true);
	return 0;
}

void pixels_handlebuffer() {
	pixels_generatebuffer();

	/* clear the interrupt bit at chan_pixels */
	dma_hw->ints1 = 1u << chan_pixels;
	/* MIND THE ^ ONE HERE*/
	
	/* re-enable itself after ... ms passed */
	add_alarm_in_us(500, alarm, NULL, true);
}

static void (*handler)();

void pixels_setup() {
	
	int sm = pio_claim_unused_sm(pio0, true);
	unsigned int offset = pio_add_program(pio0, &pixels_program);
	pixels_program_init(pio0, sm, offset, PIXEL_PIN, PIXELS_BAUD);
	pio_sm_clear_fifos(pio0, sm);
	pio_sm_restart(pio0, sm);

	/* pixels dma channel */
	chan_pixels = dma_claim_unused_channel(true);
	dma_channel_config config_pixels = dma_channel_get_default_config(
		chan_pixels);
	channel_config_set_read_increment(&config_pixels, true);
	channel_config_set_write_increment(&config_pixels, false);
	channel_config_set_transfer_data_size(&config_pixels, DMA_SIZE_32);
	channel_config_set_dreq(&config_pixels, pio_get_dreq(pio0, sm, true));

	/* & ??? */
	handler = &pixels_handlebuffer;
	
	/* this needs to be done on core1 */
	dma_channel_set_irq1_enabled(chan_pixels, true);
	irq_set_exclusive_handler(DMA_IRQ_1, (irq_handler_t) handler);
	irq_set_enabled(DMA_IRQ_1, true);

	dma_channel_configure(chan_pixels, &config_pixels,
		&pio0->txf[sm], /* destination */
		pixels_output_buffer, /* source */
		PIXELS, /* max. transfer count */
		true);

	pixels_generatebuffer();

	dma_channel_start(chan_pixels);
	pio_sm_set_enabled(pio0, sm, true);

	printf("finished pixels setup\n");
}
