// Copyright: (c) 2023 monostrider (https://monostrider.com)

#include <hardware/dma.h>
#include <hardware/pio.h>
#include <stdio.h> /* temp */
#include "encoder.pio.h"
#include "encoders.h"

volatile int32_t encoders_values[ENCODERS]; /* hardcoded to 4 */

void encoders_setup() {
	/* all statemachines share the same program, prefer PIO block 1 */
	unsigned int offset = pio_add_program(pio1, &encoder_program);
	unsigned int encoder_sm[ENCODERS];
	unsigned int encoder_chan[ENCODERS];

	for (int i = 0; i < ENCODERS; i++) {
		/* statemachine */

		encoder_sm[i] = pio_claim_unused_sm(pio1, true);

		encoder_program_init(pio1, encoder_sm[i], offset,
			ENCODERS_PIN_A[i], ENCODERS_PIN_B[i]); 

		pio_sm_set_enabled(pio1, encoder_sm[i], true);

		/* dma channel */

		encoder_chan[i] = dma_claim_unused_channel(true);
		
		dma_channel_config cfg = dma_channel_get_default_config(
			encoder_chan[i]);

		channel_config_set_read_increment(&cfg, false);
		channel_config_set_write_increment(&cfg, false);
		channel_config_set_transfer_data_size(&cfg, DMA_SIZE_32);
		/*channel_config_set_dreq(&cfg, pio_get_dreq(
			pio1, encoder_sm[i], false));*/

		/* set chain to next channel, wrap back to first if needed */
		/* FIXME: this only works when the channels that are free are
		 * all contiguous */
		unsigned int chain_to = encoder_chan[i] + 1;
		if (chain_to >= (encoder_chan[0] + ENCODERS)) {
			chain_to = encoder_chan[0];
		}

		channel_config_set_chain_to(&cfg, chain_to);

		dma_channel_configure(encoder_chan[i], &cfg,
			&encoders_values[i],       /* destination */
			&pio1->rxf[encoder_sm[i]], /* source */
			1, false);
	}

	dma_channel_start(encoder_chan[0]);
}
