# The SN-0 synthesizer and sequencer

![photo](res/preview.jpg)

> This project is discontinued, and is in a very hacky and temporary state.
> The hardware is also in a semi-functional state.
> 
> Currently I don't have the resources nor time to consitently manage a project
> or community like this.
> Contributions are not accepted at the moment.

## hacks

The hacks used due to sub-optimal hardware design include the following:

- using a seperate PIO statemachine for the I2s clock of the DAC;
- filling RX FIFO's of encoder statemachines to the brim to avoid latency;
- running DMA channels of encoders at max. speed because DREQs halt transfers.
- ...

The buttons also aren't responsive. I had to cut them to make them even work
in the first place. The AMP chip is also borked for some reason. I am also
struggling with race conditions and stuff, because I don't understand how the
pico-sdk prioritizes it's resources under the hood.

## working

I have however managed to simultaniously:

- generate sound;
- feed the sound to the DAC (line-out);
- fetch the rotary encoder values;
- drive the LED pixels;
- and scan the button matrix.

## plan

These are things I would like to fix with a new revision of the hardware.

I am also interested in designing my own programming language and compiler.
Maybe I could then use that, instead struggling with hacky C code and weird
compiler behaviour I don't understand.
*I am aware that this wouldn't be a very trivial route to take.*

## thank you

Thanks to raspberry pi for providing us their awesome library and hardware!
You can find the raspberry pi pico-sdk in addition to the tinyusb extension
inside of the ext/ directory. The pico-sdk and its extensions have different
licenses and can be found at https://github.com/raspberrypi/pico-sdk
