// Copyright: (c) 2023 monostrider (https://monostrider.com)

#ifndef _AUDIO_H
#define _AUDIO_H

extern float audio_carrier_freq;
extern float audio_mod_depth;
extern float audio_ratio;
extern float audio_amplitude;

extern volatile int32_t* writeable_output_block;
extern void audio_generatebuffer();

extern void audio_forkprocessing();

#endif
