// Copyright: (c) 2023 monostrider (https://monostrider.com)

#ifndef _DAC_H
#define _DAC_H

#define SAMPLE_RATE 48000
#define STEREO 2
#define FIRST_HALF 0
#define SECOND_HALF 2

#define DAC_PIN_SIGNAL 27
#define DAC_PIN_SELECT 26
#define DAC_PIN_CLOCK  28

extern void dac_setup();

#endif
