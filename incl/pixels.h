// Copyright: (c) 2023 monostrider (https://monostrider.com)

#ifndef _PIXELS_H
#define _PIXELS_H

#define PIXELS 16
#define PIXEL_PIN 5

#define PIXELS_BAUD 800000
extern volatile uint32_t pixels_output_buffer[PIXELS];

extern void pixels_setup();

#endif
