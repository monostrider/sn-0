// Copyright: (c) 2023 monostrider (https://monostrider.com)

#ifndef _BUTTONS_H
#define _BUTTONS_H

#define ROWS 2
#define COLS 2

extern volatile uint32_t matrix_bit_states;

#define COLS_PIN_BASE 6
#define ROWS_PIN_BASE 3 /* the two are inverted */

extern void buttons_setup();

#endif
