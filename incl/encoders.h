// Copyright: (c) 2023 monostrider (https://monostrider.com)

#ifndef _ENCODERS_H
#define _ENCODERS_H

/* the RP2040 has 8 statemachines divided over PIO blocks */
#define ENCODERS 4 /* DON'T CHANGE THIS! */

static const unsigned int ENCODERS_PIN_A[ENCODERS] = { /* 2, */ 21, 19, 17, 15 };
static const unsigned int ENCODERS_PIN_B[ENCODERS] = { /* 1, */ 20, 18, 16, 14 };
static const unsigned int ENCODERS_PIN_S[ENCODERS] = { /* 0, */ 25, 24, 23, 22 };
/* the encoder switches are handled by the CPU */

extern volatile int32_t encoders_values[ENCODERS];

extern void encoders_setup();

#endif
