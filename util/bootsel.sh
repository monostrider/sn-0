#!/bin/sh
# super user priveliges required

# Copyright: (c) 2023 monostrider (https://monostrider.com)

if [[ "$(id -u)" != "0" ]]; then
	echo "root privileges required"
	exit 1
fi

if [[ -e /dev/ttyACM0 && -r /dev/ttyACM0 ]]; then
	stty -F /dev/ttyACM0 1200
else
	echo "unable to find /dev/ttyACM0"
	exit 1
fi
