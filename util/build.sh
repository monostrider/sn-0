#!/bin/sh

# Copyright: (c) 2023 monostrider (https://monostrider.com)

cmake -DPICO_BOARD=none -DPICO_SDK_PATH=pico-sdk -B out/
