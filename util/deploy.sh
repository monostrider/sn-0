#!/bin/sh
# super user privileges required

# Copyright: (c) 2023 monostrider (https://monostrider.com)

if [[ "$(id -u)" != "0" ]]; then
	echo "root privileges required"
	exit 1
fi

device=""
#firmware=""

for paths in $(find /sys/bus/usb/devices/usb*/ -name dev); do
	syspath="${paths%/dev}"
	devname="$(udevadm info -q name -p $syspath)"
	
	if [[ -n "$(udevadm info -q property --export -p $syspath | grep "ID_SERIAL='RPI_RP2")" ]]; then
		device="$devname"
	fi
done

if [[ -n "$device" ]]; then
	echo "proceeding with device at /dev/$device"
	
	if [ -r out/main.uf2 ]; then
		echo "temporaraly mounting device"
		
		mkdir temp_mnt
		mount "/dev/$device" temp_mnt/
		cp out/main.uf2 temp_mnt/
		sync
		umount temp_mnt/
		rm -r temp_mnt/

		echo "succesfully flashed firmware to device"
	else
		echo -e "\nfirmware file not found"
		exit 1
	fi
else
	echo "unable to find device"
	exit 1
fi
