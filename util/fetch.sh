#!/bin/sh
# super user priveliges required

# Copyright: (c) 2023 monostrider (https://monostrider.com)

if [[ "$(id -u)" != "0" ]]; then
	echo "root privileges required"
	exit 1
fi

if [[ -e /dev/ttyACM0 && -r /dev/ttyACM0 ]]; then
	socat /dev/ttyACM0,raw,echo=0,crnl,escape=0x03 stdout
else
	echo "unable to monitor /dev/ttyACM0"
	exit 1
fi
